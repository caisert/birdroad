-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-03-2019 a las 16:40:42
-- Versión del servidor: 5.6.30-1
-- Versión de PHP: 7.0.31-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `birdroad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id_orders` int(15) NOT NULL,
  `date_orders` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_persons_orders` int(15) NOT NULL,
  `address_orders` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `city_orders` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `zip_orders` int(6) NOT NULL,
  `country_orders` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `phone_orders` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `status_orders` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tracking_orders` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `total_amount_orders` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `paypal_orders` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `method_orders` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `created_orders` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_orders` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_products`
--

CREATE TABLE `orders_products` (
  `id_orders_products` int(15) NOT NULL,
  `id_orders_orders_products` int(15) NOT NULL,
  `id_products_variants_orders_products` int(11) NOT NULL,
  `price_orders_products` decimal(15,2) NOT NULL,
  `quantity_orders_products` int(11) NOT NULL,
  `gift_orders_products` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `created_orders_products` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persons`
--

CREATE TABLE `persons` (
  `id_persons` int(11) NOT NULL,
  `name_persons` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `last_persons` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email_persons` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `phone_persons` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `created_persons` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persons`
--

INSERT INTO `persons` (`id_persons`, `name_persons`, `last_persons`, `email_persons`, `phone_persons`, `created_persons`) VALUES
(1, 'Admin', 'Istrador', 'admin@birdroadmiami.com', '0123 456 78 90', '2018-12-11 08:51:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persons_admin`
--

CREATE TABLE `persons_admin` (
  `id_persons_admin` int(15) NOT NULL,
  `id_persons_persons_admin` int(15) NOT NULL,
  `clave_persons_admin` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `created_persons_admin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_persons_admin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persons_admin`
--

INSERT INTO `persons_admin` (`id_persons_admin`, `id_persons_persons_admin`, `clave_persons_admin`, `created_persons_admin`, `modified_persons_admin`) VALUES
(1, 1, '123', '2018-12-16 05:34:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id_products` int(15) NOT NULL,
  `name_products` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `code_products` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `image_products` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `price_products` decimal(15,2) NOT NULL,
  `description_products` text COLLATE utf8_spanish_ci NOT NULL,
  `disabled_products` tinyint(4) NOT NULL DEFAULT '0',
  `created_products` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id_products`, `name_products`, `code_products`, `image_products`, `price_products`, `description_products`, `disabled_products`, `created_products`) VALUES
(1, 'BIRD ROAD', 'BR001W', './multimedia/products/BR001W.jpg', '74.00', 'Description Bird Road', 0, '2019-03-20 19:21:03'),
(2, 'INMIGRANT', 'BR002W', './multimedia/products/BR002W.jpg', '74.00', 'Description product 2', 0, '2019-03-20 19:21:13'),
(3, 'CALL ME', 'BR003W', './multimedia/products/BR003W.jpg', '74.00', 'Description product 3', 0, '2019-03-20 19:21:26'),
(4, 'LATIN', 'BR004W', './multimedia/products/BR004W.jpg', '79.00', 'Description product 4', 0, '2019-03-20 19:21:35'),
(5, 'OTHER', 'DE104T', './multimedia/products/DE104T.png', '78.00', 'Description product 5', 0, '2019-03-20 19:21:39'),
(6, 'ANOTHER', 'DE105T', './multimedia/products/DE105T.png', '74.00', 'Description product 6', 0, '2019-03-20 19:21:46'),
(7, 'ONE OTHER', 'DE105TP', './multimedia/products/DE105TP.png', '74.00', 'Description product 7', 0, '2019-03-20 19:21:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_attributes`
--

CREATE TABLE `products_attributes` (
  `id_products_attributes` int(15) NOT NULL,
  `name_products_attributes` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `created_products_attributes` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_attributes`
--

INSERT INTO `products_attributes` (`id_products_attributes`, `name_products_attributes`, `created_products_attributes`) VALUES
(1, 'Size', '2018-11-06 15:53:22'),
(2, 'Color', '2018-11-06 15:53:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_attr_values`
--

CREATE TABLE `products_attr_values` (
  `id_products_attr_values` int(15) NOT NULL,
  `id_products_attributes_products_attr_values` int(15) NOT NULL,
  `name_products_attr_values` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `created_products_attr_values` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_attr_values`
--

INSERT INTO `products_attr_values` (`id_products_attr_values`, `id_products_attributes_products_attr_values`, `name_products_attr_values`, `created_products_attr_values`) VALUES
(1, 1, 'S', '2018-11-06 15:53:58'),
(2, 1, 'M', '2018-11-06 15:53:58'),
(3, 1, 'L', '2018-11-06 15:54:19'),
(4, 1, 'XL', '2018-11-06 15:54:19'),
(5, 2, 'Blue', '2018-11-06 15:54:35'),
(6, 2, 'Green', '2018-11-06 15:54:35'),
(9, 2, 'Red', '2018-11-06 15:54:55'),
(10, 2, 'White', '2018-11-06 15:54:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_categories`
--

CREATE TABLE `products_categories` (
  `id_products_categories` int(15) NOT NULL,
  `id_parent_products_categories` int(15) NOT NULL,
  `name_products_categories` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `order_products_categories` int(15) NOT NULL,
  `created_products_categories` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_categories`
--

INSERT INTO `products_categories` (`id_products_categories`, `id_parent_products_categories`, `name_products_categories`, `order_products_categories`, `created_products_categories`) VALUES
(1, 0, 'DENNIM', 1, '2018-11-27 19:13:30'),
(2, 0, 'CLASSIC', 3, '2018-11-27 19:13:30'),
(3, 0, 'CASUAL', 2, '2018-11-27 19:13:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_gallery`
--

CREATE TABLE `products_gallery` (
  `id_products_gallery` int(15) NOT NULL,
  `id_slider_products_gallery` int(15) NOT NULL,
  `id_products_products_gallery` int(15) NOT NULL,
  `image_products_gallery` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `image_hd_products_gallery` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `alt_products_gallery` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Texto alternativo',
  `description_products_gallery` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `order_products_gallery` int(15) NOT NULL,
  `created_products_gallery` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `disabled_products_gallery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_gallery`
--

INSERT INTO `products_gallery` (`id_products_gallery`, `id_slider_products_gallery`, `id_products_products_gallery`, `image_products_gallery`, `image_hd_products_gallery`, `alt_products_gallery`, `description_products_gallery`, `order_products_gallery`, `created_products_gallery`, `disabled_products_gallery`) VALUES
(1, 1, 1, './multimedia/slides/BR001W.jpg', './multimedia/slides/BR001W.jpg', 'Bird Road', 'algo descriptivo para mostrar en slider', 1, '2019-03-20 15:32:35', 0),
(2, 1, 2, './multimedia/slides/BR002W.jpg', './multimedia/slides/BR002W.jpg', 'Inmigrant', 'texto descriptivo dos', 2, '2019-03-20 15:32:38', 0),
(3, 1, 3, './multimedia/slides/BR003W.jpg', './multimedia/slides/BR003W.jpg', 'Don\'t Call me mami', 'Texto descriptivo 3', 3, '2019-03-20 15:32:59', 0),
(4, 1, 4, './multimedia/slides/BR004W.jpg', './multimedia/slides/BR004W.jpg', 'Latin', 'Texto descriptivo 4', 4, '2019-03-20 15:33:06', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_images`
--

CREATE TABLE `products_images` (
  `id_products_images` int(15) NOT NULL,
  `id_products_variants_products_images` int(15) NOT NULL,
  `image_products_images` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `image_hd_products_images` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `alt_products_images` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `created_products_images` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disabled_products_images` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_images`
--

INSERT INTO `products_images` (`id_products_images`, `id_products_variants_products_images`, `image_products_images`, `image_hd_products_images`, `alt_products_images`, `created_products_images`, `disabled_products_images`) VALUES
(1, 1, './multimedia/products/BR001W.jpg', './multimedia/slides/BR001W.jpg', '', '2018-10-17 05:08:20', 0),
(2, 1, './multimedia/slides/BR001W.jpg', './multimedia/slides/BR001W.jpg', '', '2018-11-23 05:57:59', 0),
(3, 2, './multimedia/slides/BR001W.jpg', './multimedia/slides/BR002W.jpg', '', '2018-11-24 02:52:21', 0),
(4, 2, './multimedia/slides/BR001W.jpg', './multimedia/slides/BR002W.jpg', '', '2018-11-24 02:52:21', 0),
(5, 3, './multimedia/slides/BR003W.jpg', './multimedia/slides/BR003W.jpg', '', '2018-11-24 02:52:53', 0),
(6, 3, './multimedia/slides/BR003W.jpg', './multimedia/slides/BR003W.jpg', '', '2018-11-24 02:52:53', 0),
(7, 4, './multimedia/slides/BR004W.jpg', './multimedia/slides/BR004W.jpg', '', '2018-11-24 02:53:44', 0),
(8, 4, './multimedia/slides/BR004W.jpg', './multimedia/slides/BR004W.jpg', '', '2018-11-24 02:53:44', 0),
(9, 5, './multimedia/products/BR001W.jpg', '', '', '2018-11-24 02:54:23', 0),
(10, 5, './multimedia/products/BR001W.jpg', '', '', '2018-11-24 02:54:23', 0),
(11, 6, './multimedia/products/BR001W.jpg', '', '', '2018-11-24 02:54:48', 0),
(12, 6, './multimedia/products/BR001W.jpg', '', '', '2018-11-24 02:54:48', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_in_categories`
--

CREATE TABLE `products_in_categories` (
  `id_products_in_categories` int(15) NOT NULL,
  `id_products_categories_products_in_categories` int(15) NOT NULL,
  `id_products_products_in_categories` int(15) NOT NULL,
  `created_products_in_categories` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_in_categories`
--

INSERT INTO `products_in_categories` (`id_products_in_categories`, `id_products_categories_products_in_categories`, `id_products_products_in_categories`, `created_products_in_categories`) VALUES
(1, 1, 1, '2018-11-27 19:26:34'),
(2, 1, 2, '2018-11-27 19:26:34'),
(3, 1, 3, '2018-11-27 19:26:34'),
(4, 3, 4, '2018-11-27 19:26:34'),
(5, 1, 5, '2018-11-27 19:26:34'),
(6, 1, 6, '2018-11-27 19:26:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_variants`
--

CREATE TABLE `products_variants` (
  `id_products_variants` int(15) NOT NULL,
  `id_products_products_variants` int(15) NOT NULL,
  `quantity_products_variants` int(11) NOT NULL,
  `name_products_variants` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `code_products_variants` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `price_products_variants` decimal(15,2) NOT NULL,
  `disabled_products_variants` tinyint(1) NOT NULL DEFAULT '0',
  `created_products_variants` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_variants`
--

INSERT INTO `products_variants` (`id_products_variants`, `id_products_products_variants`, `quantity_products_variants`, `name_products_variants`, `code_products_variants`, `price_products_variants`, `disabled_products_variants`, `created_products_variants`) VALUES
(1, 1, 3, 'BIRD ROAD size S', 'BR001W-S', '74.00', 0, '2018-11-02 19:12:18'),
(2, 1, 3, 'BIRD ROAD size M', 'BR001W-M', '74.00', 0, '2018-11-06 19:52:59'),
(3, 2, 3, 'INMIGRANT size S', 'BR002W-S', '74.00', 0, '2018-11-06 21:15:32'),
(4, 3, 3, 'CALL ME size S', 'BR003W-S', '74.00', 0, '2018-11-24 02:42:25'),
(5, 4, 3, 'LATIN size S', 'BR004W-S', '79.00', 0, '2018-11-24 02:42:25'),
(6, 5, 0, 'OTHER size S', 'BR005W-S', '78.00', 0, '2018-11-24 02:42:25'),
(7, 6, 0, 'ANOTHER size S', 'BR006W-S', '74.00', 0, '2018-11-24 02:42:25'),
(8, 7, 0, 'Bird Road Name', 'BR007W-S', '74.00', 0, '2018-11-24 02:42:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_variants_values`
--

CREATE TABLE `products_variants_values` (
  `id_products_variants_values` int(15) NOT NULL,
  `id_products_variants_products_variants_values` int(15) NOT NULL,
  `id_products_attr_values_products_variants_values` int(15) NOT NULL,
  `created_products_variants_values` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_variants_values`
--

INSERT INTO `products_variants_values` (`id_products_variants_values`, `id_products_variants_products_variants_values`, `id_products_attr_values_products_variants_values`, `created_products_variants_values`) VALUES
(1, 1, 1, '2018-11-06 15:56:24'),
(2, 2, 2, '2018-11-06 15:56:24'),
(4, 3, 1, '2018-11-06 17:16:07'),
(5, 4, 1, '2018-11-24 15:12:09'),
(6, 5, 1, '2018-11-27 01:44:25'),
(7, 6, 1, '2018-11-27 01:44:25'),
(8, 7, 1, '2018-11-27 01:46:01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_orders`),
  ADD KEY `id_persons_orders` (`id_persons_orders`);

--
-- Indices de la tabla `orders_products`
--
ALTER TABLE `orders_products`
  ADD PRIMARY KEY (`id_orders_products`),
  ADD KEY `id_orders_orders_products` (`id_orders_orders_products`),
  ADD KEY `id_products_variants_orders_products` (`id_products_variants_orders_products`);

--
-- Indices de la tabla `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id_persons`),
  ADD UNIQUE KEY `email_persons` (`email_persons`);

--
-- Indices de la tabla `persons_admin`
--
ALTER TABLE `persons_admin`
  ADD PRIMARY KEY (`id_persons_admin`),
  ADD KEY `id_persons_persons_admin` (`id_persons_persons_admin`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_products`),
  ADD UNIQUE KEY `code_products` (`code_products`);

--
-- Indices de la tabla `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`id_products_attributes`);

--
-- Indices de la tabla `products_attr_values`
--
ALTER TABLE `products_attr_values`
  ADD PRIMARY KEY (`id_products_attr_values`),
  ADD KEY `id_products_attributes_products_attr_values` (`id_products_attributes_products_attr_values`);

--
-- Indices de la tabla `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id_products_categories`);

--
-- Indices de la tabla `products_gallery`
--
ALTER TABLE `products_gallery`
  ADD PRIMARY KEY (`id_products_gallery`),
  ADD KEY `id_products_products_galellery` (`id_products_products_gallery`);

--
-- Indices de la tabla `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id_products_images`),
  ADD KEY `id_products_variants_products_images` (`id_products_variants_products_images`);

--
-- Indices de la tabla `products_in_categories`
--
ALTER TABLE `products_in_categories`
  ADD PRIMARY KEY (`id_products_in_categories`),
  ADD KEY `id_products_categories_products_in_categories` (`id_products_categories_products_in_categories`);

--
-- Indices de la tabla `products_variants`
--
ALTER TABLE `products_variants`
  ADD PRIMARY KEY (`id_products_variants`),
  ADD UNIQUE KEY `code_products_variants` (`code_products_variants`),
  ADD KEY `id_products_products_variants` (`id_products_products_variants`);

--
-- Indices de la tabla `products_variants_values`
--
ALTER TABLE `products_variants_values`
  ADD PRIMARY KEY (`id_products_variants_values`),
  ADD KEY `id_products_variants_products_variants_values` (`id_products_variants_products_variants_values`),
  ADD KEY `id_products_attr_values_products_variants_values` (`id_products_attr_values_products_variants_values`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id_orders` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `orders_products`
--
ALTER TABLE `orders_products`
  MODIFY `id_orders_products` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persons`
--
ALTER TABLE `persons`
  MODIFY `id_persons` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `persons_admin`
--
ALTER TABLE `persons_admin`
  MODIFY `id_persons_admin` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id_products` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `id_products_attributes` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `products_attr_values`
--
ALTER TABLE `products_attr_values`
  MODIFY `id_products_attr_values` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id_products_categories` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `products_gallery`
--
ALTER TABLE `products_gallery`
  MODIFY `id_products_gallery` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id_products_images` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `products_in_categories`
--
ALTER TABLE `products_in_categories`
  MODIFY `id_products_in_categories` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `products_variants`
--
ALTER TABLE `products_variants`
  MODIFY `id_products_variants` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `products_variants_values`
--
ALTER TABLE `products_variants_values`
  MODIFY `id_products_variants_values` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`id_persons_orders`) REFERENCES `persons` (`id_persons`);

--
-- Filtros para la tabla `orders_products`
--
ALTER TABLE `orders_products`
  ADD CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`id_orders_orders_products`) REFERENCES `orders` (`id_orders`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`id_products_variants_orders_products`) REFERENCES `products_variants` (`id_products_variants`);

--
-- Filtros para la tabla `persons_admin`
--
ALTER TABLE `persons_admin`
  ADD CONSTRAINT `persons_admin_ibfk_1` FOREIGN KEY (`id_persons_persons_admin`) REFERENCES `persons` (`id_persons`);

--
-- Filtros para la tabla `products_attr_values`
--
ALTER TABLE `products_attr_values`
  ADD CONSTRAINT `products_attr_values_ibfk_1` FOREIGN KEY (`id_products_attributes_products_attr_values`) REFERENCES `products_attributes` (`id_products_attributes`);

--
-- Filtros para la tabla `products_gallery`
--
ALTER TABLE `products_gallery`
  ADD CONSTRAINT `products_gallery_ibfk_1` FOREIGN KEY (`id_products_products_gallery`) REFERENCES `products` (`id_products`);

--
-- Filtros para la tabla `products_images`
--
ALTER TABLE `products_images`
  ADD CONSTRAINT `products_images_ibfk_1` FOREIGN KEY (`id_products_variants_products_images`) REFERENCES `products_variants` (`id_products_variants`);

--
-- Filtros para la tabla `products_in_categories`
--
ALTER TABLE `products_in_categories`
  ADD CONSTRAINT `products_in_categories_ibfk_1` FOREIGN KEY (`id_products_categories_products_in_categories`) REFERENCES `products` (`id_products`),
  ADD CONSTRAINT `products_in_categories_ibfk_2` FOREIGN KEY (`id_products_categories_products_in_categories`) REFERENCES `products_categories` (`id_products_categories`);

--
-- Filtros para la tabla `products_variants`
--
ALTER TABLE `products_variants`
  ADD CONSTRAINT `products_variants_ibfk_1` FOREIGN KEY (`id_products_products_variants`) REFERENCES `products` (`id_products`);

--
-- Filtros para la tabla `products_variants_values`
--
ALTER TABLE `products_variants_values`
  ADD CONSTRAINT `products_variants_values_ibfk_1` FOREIGN KEY (`id_products_variants_products_variants_values`) REFERENCES `products_variants` (`id_products_variants`),
  ADD CONSTRAINT `products_variants_values_ibfk_2` FOREIGN KEY (`id_products_attr_values_products_variants_values`) REFERENCES `products_attr_values` (`id_products_attr_values`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
