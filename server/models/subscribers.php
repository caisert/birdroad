<?php
/**
 * @class SubscribersModel
 * @constructor PDO
 * @description Modelo de la entidad Suscriptores
 */
class SubscribersModel
{
    // Declaración de una propiedad
    private $db;

    // Constructor
    public function __construct() {
        include 'database.php';
        $this->db = dbInit();
    }
    // Declaración de un método
    public function mostrarVar() {
        echo $this->var;
    }
    // $this->db->errorCode() 
    public function readSubscribers() {
        $statement = $this->db->prepare('SELECT * FROM persons');
        $statement->execute();
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            while ($fila = $statement->fetchObject()) {
              $load[] = $fila;
            }
            $res['load'] = $load;
        }
        return $res;
    }

    public function createSubscriber($subscriber) {
        $statement = $this->db->prepare('INSERT INTO persons (`name_persons`,`email_persons`) VALUES (:name, :email)');
        $statement->execute([
            ':name' => $subscriber['name'],
            ':email' => $subscriber['email']
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            // $res['error'] = $statement->errorInfo();
            if ($statement->errorInfo()[1] == 1062) {
                $res['error'] = "Your email is already subscribed.";
            } else {
                $res['error'] = "A error ocurred. Please try again or leave a message in the contact form.";
            }
            
        } else {
            $load = [];
            while ($fila = $statement->fetchObject()) {
              $load[] = $fila;
            }
            $res['load'] = $load;
        }
        return $res;
    }

    public function changePassword($password) {
        $statement = $this->db->prepare('UPDATE persons_admin SET `clave_persons_admin`=:new WHERE id_persons_admin=:id_persons_admin AND clave_persons_admin=:current');
        $statement->execute([
            ':current' => $password['current'],
            ':new' => $password['new'],
            ':id_persons_admin' => $_SESSION["usr_id"]
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->rowCount() == 0) {
            $res['error'] = $statement->errorInfo();
        }
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
            
        } elseif ($statement->rowCount() == 0) {
            $res['error'] = "Invalid current password.";
        } else {
            $load = [];
            
            $res['load'] = "success";
        }
        return $res;
    }
}
?>