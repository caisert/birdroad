<?php

function limpiarCadena($valor){
    $valor = str_ireplace("SELECT","",$valor);
    $valor = str_ireplace("INSERT","",$valor);
    $valor = str_ireplace("COPY","",$valor);
    $valor = str_ireplace("DELETE","",$valor);
    $valor = str_ireplace("DROP","",$valor);
    $valor = str_ireplace("DUMP","",$valor);
    $valor = str_ireplace(" OR ","",$valor);
    $valor = str_ireplace("%","",$valor);
    $valor = str_ireplace("LIKE","",$valor);
    $valor = str_ireplace("--","",$valor);
    $valor = str_ireplace("^","",$valor);
    $valor = str_ireplace("[","",$valor);
    $valor = str_ireplace("]","",$valor);
    $valor = str_ireplace("\\","",$valor);
    $valor = str_ireplace("!","",$valor);
    $valor = str_ireplace("¡","",$valor);
    $valor = str_ireplace("<?","",$valor);
    $valor = str_ireplace("?>","",$valor);
    $valor = str_ireplace("=","",$valor);
    $valor = str_ireplace("&","",$valor);
    $valor = str_ireplace("'","",$valor);
    $valor = str_ireplace("\"","",$valor);
    $valor = str_ireplace("<","",$valor);
    $valor = str_ireplace(">","",$valor);

    return $valor;
}
// Limpia las entradas por POST o GET
$input = array(); 
 foreach ($_GET as $key => $input) {
     $_GET[$key] = limpiarCadena($input); 
 }
$input = array(); 
foreach ($_POST as $key => $input) {
    $_POST[$key] = limpiarCadena($input); 
}
$input = array(); 
foreach ($_REQUEST as $key => $input) {
    $_REQUEST[$key] = limpiarCadena($input); 
}
$input = array(); 
foreach ($_COOKIE as $key => $input) {
    $_COOKIE[$key] = limpiarCadena($input); 
}

//seguridad general
function check_usr_ok(){
    if (!isset($_SESSION['usr_id'])) {
        $r=false;
        $e=null;
    	if($_GET['e'] == "login") {
		    $user = strip_tags($_POST['user']);
		    $con = strip_tags($_POST['pass']);
		    $e = "No llegó nada.";
		    if ($user && $con){
                require_once '../server/models/database.php';
                $db = dbInit();
		        // $user = $db->escape_string(strtolower(htmlentities($user)));

                $statement = $db->prepare('SELECT id_persons_admin, email_persons, clave_persons_admin FROM persons JOIN persons_admin ON id_persons=id_persons_persons_admin WHERE email_persons = :user');
                $statement->execute([
                    ':user' => $user
                ]);
                // $statement->errorInfo();
                $number_of_rows = $statement->rowCount();

                $e="Usuario o clave incorrect";
                
		        if ($number_of_rows > 0){
		            $row = $statement->fetch();
	                if($row["clave_persons_admin"] == $con){
	                    $_SESSION["usr_id"] = $row['id_persons_admin'];
	                    
	                    $e="Sesion iniciada";
	                    $r=true;
					    $json = array("r"=>$r,"e"=>$e);
					    echo json_encode($json);
	                    exit;
	                } else{
	                    $e .= "a";
	                }
		        } else {
		            $e .= "o";
		        }
		    }
		    $json = array("r"=>$r,"e"=>$e);
		    echo json_encode($json);
		    exit;
		}
	    $json = array("r"=>$r,"e"=>$e);
	    echo json_encode($json);
	    exit;
	}
}
?>