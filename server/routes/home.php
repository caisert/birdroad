<?php
/**
 * @function router
 * @description Rutas de la entidad home
 * @param {string} $route Ruta solicitada
 * @param {string} $controllersDir ubicacion de la carpeta de controladores
 * @param {string} $modelsDir Ubicacion de la carpeta de modelos
 */
function router ($route, $controllersDir, $modelsDir) {
    require_once($controllersDir.'products'.'.php');
    include_once('../server/routes/errors.php');
    $method = $_SERVER['REQUEST_METHOD'];
    $products = new ProductsController();

    switch ($route) {
        case '/':
            switch ($method) {
                case 'GET':
                    $id = $_GET['id'];
                    return $products->readProductsSlides($id);
                    break;
                default:
                    /**
                     * @todo Crear manejador de Not Found 404
                     */
                    return $res['error'] = "Método inválido. rh:".$method;
                    break;
            }
            break;
        default:
            # code...
            $res['error'] = "Ruta inválida. rh: ".$route;
            return $res;
            break;
    }
}
?>