<?php
session_start();
/**
 * Controlador principal de la aplicación
 */

$app_conf = json_decode(file_get_contents('../localconf/config.json'), true);

/**
 * @function Muestra los errores al desarrollador
 * @param {object} $app_conf Objeto de configuración de la app
 */
function showErrors($app_conf) {
    if ($app_conf['enviroment'] === 'development') {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
    }
}
showErrors($app_conf);

$method = $_SERVER['REQUEST_METHOD'];
$route = $_GET['r'];
$entity = $_GET['e'];
// Ubicacion de carpetas
$routesDir = $app_conf['routesDir'];
$controllersDir = $app_conf['controllersDir'];
$modelsDir = $app_conf['modelsDir'];

switch ($entity) {
    case 'home':
        include($routesDir.'home'.'.php');
        $res = router($route, $controllersDir, $modelsDir);
        break;
    case 'products':
        include($routesDir.'products'.'.php');
        $res = router($route, $controllersDir, $modelsDir);
        break;
    case 'subscribers':
        include($routesDir.'subscribers'.'.php');
        $res = router($route, $controllersDir, $modelsDir);
        break;
    case 'orders':
        include($routesDir.'orders'.'.php');
        $res = router($route, $controllersDir, $modelsDir);
        break;
    case 'login':
        require_once('../server/utils/seguridad.php');
        check_usr_ok();
        break;
    case 'logout':
        session_destroy();
        $res = [];
        break;
    default:
        $res['error'] = "Ruta invalida";
        break;
}

echo json_encode($res);

?>